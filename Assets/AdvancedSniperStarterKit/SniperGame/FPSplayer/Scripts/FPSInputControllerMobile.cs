﻿using UnityEngine;
using System.Collections;

public class FPSInputControllerMobile : MonoBehaviour {

	private GunHanddle gunHanddle;
	private FPSController FPSmotor;
	
	public TouchScreenVal touchMove;
	public TouchScreenVal touchAim;
	public TouchScreenVal touchShoot;
	public TouchScreenVal touchZoom;
	public TouchScreenVal touchJump;
	
	public Texture2D ImgButton;
	public float TouchSensMult = 0.05f;

	
	void Start(){
		Application.targetFrameRate = 60;
	}
	void Awake ()
	{
		FPSmotor = GetComponent<FPSController> ();		
		gunHanddle = GetComponent<GunHanddle> (); 
	}

	void Update ()
	{

		Vector2 aimdir = touchAim.OnDragDirection(true);
		FPSmotor.Aim(new Vector2(ControlFreak2.CF2Input.GetAxisRaw("Mouse X"),ControlFreak2.CF2Input.GetAxisRaw("Mouse Y"))*TouchSensMult);

	}


	public void ZoomIn(){
		gunHanddle.ZoomAdjust (1);
	}

	public void ZoonOut(){
		gunHanddle.ZoomAdjust (-1);
	}

	public void Zoom(){
		gunHanddle.Zoom ();
	}
	public void Fire(){
		gunHanddle.Shoot ();
	}
	void OnGUI(){
		
//		touchMove.Draw();
//		touchAim.Draw();
//		touchShoot.Draw();
//		touchZoom.Draw();
		
	}
}
