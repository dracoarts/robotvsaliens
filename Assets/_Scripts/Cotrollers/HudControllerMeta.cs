﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HudControllerMeta : GameController {
	private FPSInputControllerMobile controller = null;
	private HudMenu menuController;
	private float currentValue = 0;
	public Text LevelTime;
	Level l;
	public GameObject Scope;
	public GameObject ZoomInOut;
	public GameObject TouchZone;
	void OnEnable(){
		menuController = GameObject.FindObjectOfType<HudMenu> ();
		ZoomInOut.SetActive (false);
		Scope.SetActive (false);
		l = GameObject.FindObjectOfType<Level> ();
	}


	void Update(){
		if (controller == null) {
			controller = GameObject.FindObjectOfType<FPSInputControllerMobile> ();
		}

		if (menuController == null) {
			menuController =  GameObject.FindObjectOfType<HudMenu> ();
		}

		float min = l.TotalTime / 60;
		float sec = l.TotalTime % 60;
		LevelTime.text = string.Format ("{0:00} : {1:00}", min, sec);
			
	}

	public void PauseGame(){
		GameManager.Instance.LevelPaused ();
		menuController.GamePause ();
	}

	public void Fire(){
		controller.Fire ();
	}

	public void ZoomIn(){
		controller.ZoomIn ();
	}

	public void ZoomOut(){
		controller.ZoonOut ();
	}

	public void Aim(){
		controller.Zoom ();

		Scope.SetActive (!Scope.activeInHierarchy);
		ZoomInOut.SetActive (!ZoomInOut.activeInHierarchy);
	}

	public override void OnLevelComplete ()
	{
		if (Scope.activeInHierarchy) {
			controller.Zoom ();
			Scope.SetActive (false);
			ZoomInOut.SetActive (false);
		}
	}
	public override void OnLevelFailed ()
	{
		if (Scope.activeInHierarchy) {
			controller.Zoom ();
			Scope.SetActive (false);
			ZoomInOut.SetActive (false);
		}
	}
	public override void OnLevelPaused ()
	{
		if (Scope.activeInHierarchy) {
			controller.Zoom ();
			Scope.SetActive (false);
			ZoomInOut.SetActive (false);
		}
	}
}
