﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelComplete : IMenuController {

	public Text levelCompleteBonus;
	public GameObject RatePanel;
	void OnEnable(){
		//this.setPreviousMenu (null);
		//GameObject.FindGameObjectWithTag("Player").SetActive(false);
//		GAManager.Instance.ProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Complete,"Alien",DataCentre.CurrentLevel.ToString());
		DataCentre.LevelCompleteCounters++;

		if (DataCentre.LevelCompleteCounters % 3 == 2) {
			RatePanel.SetActive (true);
		}

		int a = Random.Range(100,500);
		DataCentre.Coins += a * DataCentre.CurrentLevel;
		levelCompleteBonus.text = (a * DataCentre.CurrentLevel).ToString ();

		//AdsManager.Instance.ShowAd (AdsManager.DracoAdType.NORMAL_MONETIZATION, (bool r) => {
		//});


	}


	public void LevelResart(){
		MenuManager.Instance.LoadScene (2);
		//GAManager.Instance.ProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Start,"Alien",DataCentre.CurrentLevel.ToString());
		//if (AdsManager.Instance.CanShowAd ()) {
		//	AdsManager.Instance.ShowAd (AdsManager.DracoAdType.NORMAL_MONETIZATION, (bool result) => {
		//	});
		//}
	}

	public void NextLevel(){
		DataCentre.CurrentLevel = Mathf.Clamp (DataCentre.CurrentLevel + 1, 1, 10);
		//GAManager.Instance.ProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Start,"Alien",DataCentre.CurrentLevel.ToString());
		MenuManager.Instance.LoadScene (2);
		//if (AdsManager.Instance.CanShowAd ()) {
		//	AdsManager.Instance.ShowAd (AdsManager.DracoAdType.NORMAL_MONETIZATION, (bool result) => {
		//	});
		//}
	}

	public void GoHome(){
		MenuManager.Instance.LoadScene (1);
		//if (AdsManager.Instance.CanShowAd ()) {
		//	AdsManager.Instance.ShowAd (AdsManager.DracoAdType.NORMAL_MONETIZATION, (bool result) => {
		//	});
		//}
	}

	void Update(){
	}


	public void OpenRateUs(){
		Debug.Log ("Agya bhai andr");
		Application.OpenURL (DataCentre.GAME_URL);
	}
}
