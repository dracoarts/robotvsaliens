﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PopupController : GameController {

	public Text InstructionPopupText;
	public GameObject GOtoActivate;

	public GameObject FUllBG;
	string Instruction;


	private bool isPaused = false;
	public void ActivatePopup(string instruction){
		Time.timeScale = 0;
		Instruction = "";
		Instruction = instruction;
		InstructionPopupText.text = "";
		StartCoroutine (ShowText ());
	//	InstructionPopupText.text = instruction;
		GOtoActivate.SetActive (true);
		FUllBG.SetActive (true);

	}

	public void DisablePopup(){
		Time.timeScale = 1;
		InstructionPopupText.text = "";
		FUllBG.SetActive (false);
		GOtoActivate.SetActive (false);
	}


	IEnumerator ShowText(){
		char[] arr = Instruction.ToCharArray ();

		foreach (char c in arr) {
			InstructionPopupText.text += c;
			//yield return new WaitWhile (() => (isPaused == true));
			yield return new WaitForSecondsRealtime (0.05f);
		}
	}


	public override void OnLevelRestart ()
	{
		StopAllCoroutines ();
		DisablePopup ();
	}
	public override void OnLevelPaused ()
	{
		isPaused = true;
	}
	public override void OnLevelResume ()
	{
		isPaused = false;
	}

	private void ShowScript() {

	}

}
