﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudMenu : IMenuController {



	public IMenuController LevelCompleteController;
	public IMenuController LevelFailController;
	public IMenuController PauseMenuController;

	public GameObject Controllers;
	private GameObject GunController;

	void OnEnable(){
		this.setPreviousMenu (null);
//		AdsManager.Instance.ShowTopBannerView ();
	}

	void OnDisable(){
	//	AdsManager.Instance.HideBannerTop ();
	}
	public void OnSceneLoaded(){
		if (Application.loadedLevel != 1) {
			GunController = Instantiate (Controllers, new Vector3 (1000, 1000, 1000), Quaternion.identity) as GameObject;

		}
	}

	public override void OnLevelResume ()
	{
	//	GunController = Instantiate (Controllers, new Vector3 (1000, 1000, 1000), Quaternion.identity) as GameObject;
	}
	public override void OnLevelStart (){
	}




	void Update(){
	}
	public override void OnLevelPaused ()
	{
		Time.timeScale = 0;
		this.SwitchNextMenu (PauseMenuController);
	}

	public void GamePause(){
		
	}
	public override void OnLevelComplete ()
	{
		Destroy (GunController.gameObject);
		this.SwitchNextMenu (LevelCompleteController);
	}

	public override void OnLevelFailed ()
	{
		Destroy (GunController.gameObject);
		this.SwitchNextMenu (LevelFailController);
	}




}
