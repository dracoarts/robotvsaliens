﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelSelection : IMenuController {

	public Button []Locks;

	public void PlayLevel(int levelNo){
		DataCentre.CurrentLevel = levelNo;
//		GAManager.Instance.ProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Start,"Alien",DataCentre.CurrentLevel.ToString());
		MenuManager.Instance.LoadScene (2);
		//if (AdsManager.Instance.CanShowAd ()) {
		//	AdsManager.Instance.ShowAd (AdsManager.DracoAdType.NORMAL_MONETIZATION, (bool result) => {
		//	});
		//}
	}

	void OnEnable(){
		Init ();
	}


	void Init(){
		for(int i=0;i<10;i++) {
			if (DataCentre.UnlockLevels [i]) {
				Locks [i].interactable = true;
			} else {
				Locks [i].interactable = false;
			}
		}
	}


}
