﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : IMenuController {


	void OnEnable(){
		
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void OpenRateUs(){
//		GAManager.Instance.LogDesignEvent ("Go For Rate");
		Application.OpenURL (DataCentre.GAME_URL);
	}

	public void OpenMoreGames(){
		//Application.OpenURL ("https://play.google.com/store/apps/developer?id=Invincible+Gaming+Studios");
	}

	public void NoAds(){
		StoreManager.Instance.BuyProduct ("com.taptash.robot.transformer.machine.alien.war.ship.superhero.noads");
	}

	public void RestoreAll(){
		StoreManager.Instance.RestorePurchases ();
	}
}
