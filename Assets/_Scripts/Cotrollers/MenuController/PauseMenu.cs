﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : IMenuController {

	private HudControllerMeta meta;

	void OnEnable(){
		meta = GameObject.FindObjectOfType<HudControllerMeta> ();


	}

	public void ResumeGame(){
		GameManager.Instance.LevelResume ();
		Time.timeScale = 1;
		this.SwitchPreviousMenu ();
	}
	public void RestartLevel(){
		Time.timeScale = 1;
		MenuManager.Instance.LoadScene (2);
//		GAManager.Instance.ProgressionEvent(GameAnalyticsSDK.GAProgressionStatus.Start,"Alien",DataCentre.CurrentLevel.ToString());
		//if (AdsManager.Instance.CanShowAd ()) {
		//	AdsManager.Instance.ShowAd (AdsManager.DracoAdType.NORMAL_MONETIZATION, (bool result) => {
		//	});
		//}
	}
	public void GoHome(){
		if (meta != null) {
			Destroy (meta.gameObject);
		}
		Time.timeScale = 1;
		MenuManager.Instance.LoadScene (1);
		//if (AdsManager.Instance.CanShowAd ()) {
		//	AdsManager.Instance.ShowAd (AdsManager.DracoAdType.NORMAL_MONETIZATION, (bool result) => {
		//	});
		//}
	}


	void Update(){
		if (Input.GetKey (KeyCode.Escape)) {
			ResumeGame ();
		}
	}
}
