﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WeaponSelectionController : IMenuController {

	public Text TotalCash;
	public Text priceTag;
	public GameObject sniperMenu;


	public GameObject Level1Lock;
	public GameObject Level2Lock;


	public GameObject Level1Unlocker;
	public GameObject Level2Unlocker;


	public GameObject[] ScopePrices;
	public GameObject[] RangePrices;


	public GameObject WeapomBuyButton;
	private GameObject SMJ;
	void Update () {
		TotalCash.text = "$ " + DataCentre.Coins.ToString ();
		if (!DataCentre.unlockedWeapons [DataCentre.CurrentSniper - 1]) {
			priceTag.text = "$ " + DataCentre.WeaponPrices [DataCentre.CurrentSniper - 1].ToString ();
		} else {
			priceTag.text = "";
		}
	}


	void OnEnable(){
		SMJ = Instantiate (sniperMenu, new Vector3 (5000, 5000, 5000), Quaternion.identity);
		DataCentre.CurrentSniper = 1;
		GameObject.FindObjectOfType<SniperMenuMeta> ().ActivateSniper (DataCentre.CurrentSniper);
		WeapomBuyButton.SetActive (false);
	}


	void OnDisable(){
		Destroy (SMJ);
		if (!DataCentre.unlockedWeapons [DataCentre.CurrentSniper - 1]) {
			DataCentre.CurrentSniper = 1;
		} else {
		}
	}

	public void BuyWeapon(){
		if (DataCentre.Coins >= DataCentre.WeaponPrices[DataCentre.CurrentSniper -1]) {
			DataCentre.unlockedWeapons [DataCentre.CurrentSniper - 1] = true;
			DataCentre.Coins -= DataCentre.WeaponPrices[DataCentre.CurrentSniper -1];
			WeapomBuyButton.SetActive (false);
		}
	}
		
	public void UpdateScope(int level){
		if (DataCentre.WeaponScopeUpgrades [DataCentre.CurrentSniper - 1] + 1 == level) {
			if (DataCentre.Coins >= DataCentre.WeaponUpgradePrices [DataCentre.WeaponScopeUpgrades [DataCentre.CurrentSniper - 1]]) {
				DataCentre.WeaponScopeUpgrades [DataCentre.CurrentSniper - 1]++;
				DataCentre.Coins -= DataCentre.WeaponUpgradePrices [DataCentre.WeaponScopeUpgrades [DataCentre.CurrentSniper - 1]];

			}
		}
	}

	public void UpgradeRange(int level){


		if (DataCentre.WeaponRangeUpgrades [DataCentre.CurrentSniper - 1] + 1 == level) {
			if (DataCentre.Coins >= DataCentre.WeaponUpgradePrices [DataCentre.WeaponRangeUpgrades [DataCentre.CurrentSniper - 1]]) {
				DataCentre.WeaponRangeUpgrades [DataCentre.CurrentSniper - 1]++;
				DataCentre.Coins -= DataCentre.WeaponUpgradePrices [DataCentre.WeaponRangeUpgrades [DataCentre.CurrentSniper - 1]];
			}
		}
	}
	public void NextWeapon(){
		DataCentre.CurrentSniper = Mathf.Clamp (DataCentre.CurrentSniper + 1, 1, 3);
		GameObject.FindObjectOfType<SniperMenuMeta> ().ActivateSniper (DataCentre.CurrentSniper);

		if (!DataCentre.unlockedWeapons [DataCentre.CurrentSniper - 1]) {
			WeapomBuyButton.SetActive (true);
		} else {
			WeapomBuyButton.SetActive (false);
		}
	}

	public void PrevWeapon(){
		DataCentre.CurrentSniper = Mathf.Clamp (DataCentre.CurrentSniper - 1, 1, 3);
		GameObject.FindObjectOfType<SniperMenuMeta> ().ActivateSniper (DataCentre.CurrentSniper);

		if (!DataCentre.unlockedWeapons [DataCentre.CurrentSniper - 1]) {
			WeapomBuyButton.SetActive (true);
		} else {
			WeapomBuyButton.SetActive (false);
		}
	}
}
