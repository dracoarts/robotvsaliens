﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnReviveReseter : GameController {
	private Vector3 startingPos;
	private Quaternion startingRot;


	void OnEnable(){
		startingPos = this.transform.localPosition;
		startingRot = this.transform.localRotation;
	}



	public override	void OnLevelRevive(){

		this.transform.localPosition = startingPos;
		this.transform.localRotation = startingRot;
	}
}
