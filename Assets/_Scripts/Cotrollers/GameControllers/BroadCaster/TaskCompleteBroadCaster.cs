﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TaskCompleteBroadCaster : GameController {
	/*
	 * Trigger Event Based Task
	 * Add This Script On Collider With Trigger Entered
	 * 
	 */
	public string PlayerTag;

	private int timer = 4;

	public GameObject TimerBG;
	public Image FillImage;
	public Text label;

	private bool gamePaused = false;

	void OnEnable(){
		timer = 4;
	}

	public void OnTriggerEnter(Collider col){
		PlayerUniqueTag Tage = col.gameObject.GetComponent<PlayerUniqueTag> ();
		if (Tage != null) {
			if (PlayerTag.Equals(Tage.Tag)) {
			//	timer = 3;
				StartCoroutine (StartTimer ());
			}
		}
	}

	public void OnTriggerExit(Collider col){
		PlayerUniqueTag Tage = col.gameObject.GetComponent<PlayerUniqueTag> ();
		if (Tage != null) {
			if (PlayerTag.Equals(Tage.Tag)) {
				StopAllCoroutines ();
				timer = 4 ;
				FillImage.fillAmount = 0;
				TimerBG.SetActive (false);
			}
		}
	}



	IEnumerator StartTimer(){
		TimerBG.SetActive (true);
		FillImage.fillAmount = 0;
		timer = 3;
		while (timer > 0) {
			
			yield return new WaitForSeconds (1);
			if (!gamePaused) {
				timer--;
			}
		}
		GameManager.Instance.TaskCompleted ();

	}

	public void OnTriggerStay(Collider col){
	}


	void Update(){
		if (label != null) {
			if (timer == 4) {
				label.text = "";
			} else {
				label.text = timer.ToString();
			}
		}

		if (FillImage.gameObject.activeInHierarchy) {
			if(!gamePaused)
				FillImage.fillAmount = Mathf.MoveTowards (FillImage.fillAmount, 1, 0.32f * Time.deltaTime);
		}
	}

	public override void OnLevelPaused ()
	{
		base.OnLevelPaused ();
		gamePaused = true;
	}
	public override void OnLevelResume ()
	{
		base.OnLevelResume ();
		gamePaused = false;
	}

}
