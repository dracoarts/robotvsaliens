﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RobotHudControlMeta : MonoBehaviour {
	public Image healthValue;
	public Text totalBulletsLeft;
	public Text totalMissileLeft;


	private RobotPlayer player = null;


	public void PauseGame(){
		GameManager.Instance.LevelPaused ();
	}

	void Update(){
		if (player == null) {
			player = GameObject.FindObjectOfType<RobotPlayer> ();
		} else {
			totalBulletsLeft.text = player.totalBulltets.ToString();
			totalMissileLeft.text = player.totalMissles.ToString();
			healthValue.fillAmount = Mathf.MoveTowards(healthValue.fillAmount,player.totalHealth / 100,0.1f*Time.deltaTime);
		}

		if (ControlFreak2.CF2Input.GetKeyDown (KeyCode.P)) {
			PauseGame ();
		}
	}







}
