﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftRightButtons : MonoBehaviour {

	void Update () {
		if (ControlFreak2.CF2Input.GetKey (KeyCode.LeftArrow)) {
			Controller.Steer = Mathf.MoveTowards (Controller.Steer, -1, 7 * Time.deltaTime);
		} else if (ControlFreak2.CF2Input.GetKey (KeyCode.RightArrow)) {
			Controller.Steer = Mathf.MoveTowards (Controller.Steer, 1, 7 * Time.deltaTime);
		} else {
			Controller.Steer = Mathf.MoveTowards (Controller.Steer, 0, 7 * Time.deltaTime);
		}
	}
}
