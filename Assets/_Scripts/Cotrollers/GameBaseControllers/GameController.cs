﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
	public virtual void OnLevelStart(){
	}
	public virtual void OnLevelFailed(){
	}
	public virtual void OnLevelRestart(){
	}
	public virtual void OnLevelRevive(){
	}
	public virtual void OnLevelPaused(){
	}
	public virtual void OnLevelResume(){
	}
	public virtual void OnLevelComplete(){
	}
	public virtual void OnTaskComplete(){
	}
}
