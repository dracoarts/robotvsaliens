﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidBoxCollidingGround : GameController {

	public static int TotalFall = 0;

	private bool Fallen=false;


	void OnEnable(){
		TotalFall = 0;
	}

	void OnCollisionStay(Collision col){
		if (col.collider.gameObject.CompareTag ("Terrain")) {
			if (!Fallen) {
				Fallen = true;
				TotalFall++;
				if (TotalFall >= 2) {
					GameManager.Instance.LevelFailed ();
				}
			}
		}
	}
	public override void OnLevelPaused ()
	{
		base.OnLevelPaused ();
		this.GetComponent<Rigidbody> ().isKinematic = true;

	}

	public override void OnLevelResume ()
	{
		base.OnLevelResume ();
		this.GetComponent<Rigidbody> ().isKinematic = false;
	}
	public override void OnLevelRevive ()
	{
		Fallen = false;
		this.GetComponent<Rigidbody> ().velocity = Vector3.zero;
		TotalFall = 0;
	}
}
