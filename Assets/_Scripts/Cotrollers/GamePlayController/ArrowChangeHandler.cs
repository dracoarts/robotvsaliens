﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowChangeHandler : MonoBehaviour {

	public string TagToCheck;
	public ArrowController arrowController;
	void OnTriggerEnter(Collider col){
		PlayerUniqueTag tags = col.gameObject.GetComponent<PlayerUniqueTag> ();

		if (tags != null) {
			if (tags.Tag.Equals(TagToCheck)) {
				arrowController.ChangeTarget ();
				this.GetComponent<Collider> ().enabled = false;
			}
		}
	}
}
