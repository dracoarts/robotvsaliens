﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour {


	public GameObject[] Targets;
	private int currentTarget = 0;
	void OnEnable(){
		currentTarget = 0;
	}
	public void Update(){
		if (currentTarget < Targets.Length) {
			var targetRotation = Quaternion.LookRotation (Targets [currentTarget].transform.position - transform.position);
			transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, 1 * Time.deltaTime);
		}
	}


	public void ChangeTarget(){
		currentTarget++;
	}
}
