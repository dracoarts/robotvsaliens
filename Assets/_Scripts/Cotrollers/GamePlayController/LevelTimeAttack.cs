﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelTimeAttack : GameController {

	public float TotalTime = 100;

	public float timeLeft;

	public Text timmer;

	void OnEnable(){
		timeLeft = TotalTime;
		StartCoroutine (StartTimmer ());
	}

	IEnumerator StartTimmer(){
		while (timeLeft > 0) {
			yield return new WaitForSeconds (1);
			timeLeft--;
		}
		GameManager.Instance.LevelFailed ();
	}

	void Update(){
		if (timmer!=null) {
			int min = (int)(timeLeft / 60);
			int sec = (int)(timeLeft % 60);
			timmer.text = string.Format ("{0:00} : {1:00}", min, sec);
			//timmer.text = ((int)(timeLeft / 60)).ToString () + " : " + ((int)(timeLeft % 60)).ToString ();
		}
	}
	public override void OnLevelRevive ()
	{
		base.OnLevelRevive ();
		timeLeft = TotalTime;
		StartCoroutine (StartTimmer ());

	}

	public override void OnLevelPaused ()
	{
		base.OnLevelPaused ();
		StopAllCoroutines ();
	}

	public override void OnLevelResume ()
	{
		base.OnLevelResume ();
		StartCoroutine (StartTimmer ());
	}

	public override void OnLevelComplete ()
	{
		base.OnLevelComplete ();
		StopAllCoroutines ();
	}

	public override void OnLevelFailed ()
	{
		base.OnLevelFailed ();
		StopAllCoroutines ();
	}


}
