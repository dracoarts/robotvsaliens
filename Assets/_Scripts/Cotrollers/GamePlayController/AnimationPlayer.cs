﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlayer : MonoBehaviour {

	public AnimationsController controller;
	public string PlayerTag;
	public string TriggerEnterAnimationTag;
	public string TriggerExitAnimationTag;

	public void OnTriggerEnter(Collider col){
		PlayerUniqueTag Tage = col.gameObject.GetComponent<PlayerUniqueTag> ();
		if (Tage != null) {
			if (PlayerTag.Equals (Tage.Tag)) {
				controller.PlayAnimation (TriggerEnterAnimationTag);
			}
		}
	}

	void OnTriggerExit(Collider col){
		PlayerUniqueTag Tage = col.gameObject.GetComponent<PlayerUniqueTag> ();
		if (Tage != null) {
			if (PlayerTag.Equals (Tage.Tag)) {
				controller.PlayAnimation (TriggerExitAnimationTag);
			}
		}
	}
}
