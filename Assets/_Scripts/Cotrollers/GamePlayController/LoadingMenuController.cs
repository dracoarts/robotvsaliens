﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class LoadingMenuController : IMenuController {


	public UnityEvent OnLevelLoaded;


	void OnDisable(){
		OnLevelLoaded.Invoke ();
	}

	void OnEnable(){
		this.setPreviousMenu (null);
	}
}
