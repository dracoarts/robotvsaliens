﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsController : MonoBehaviour {

	Animator _animator;

	public string[] TriggerList;
	void OnEnable(){
		_animator = this.GetComponent<Animator>();
	}



	public void PlayAnimation(string Trigger){
		ResetAll ();
		_animator.SetTrigger (Trigger);
	}

	public void ResetAnimationTrigger(string Trigger){
		ResetAll ();
		_animator.ResetTrigger (Trigger);
	}

	void ResetAll(){
		foreach (string s in TriggerList) {
			_animator.ResetTrigger (s);
		}
	}
}
