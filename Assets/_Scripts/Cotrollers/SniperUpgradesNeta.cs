﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperUpgradesNeta : MonoBehaviour {

	private GunMecanim gun;


	public void OnEnable(){
		gun = this.GetComponent<GunMecanim> ();
		if(DataCentre.WeaponScopeUpgrades[DataCentre.CurrentSniper - 1] == 0){
			gun.ZoomFOVLists = new float [3];
			gun.ZoomFOVLists [0] = 30;
			gun.ZoomFOVLists [1] = 20;
			gun.ZoomFOVLists [2] = 10;
		}

		if(DataCentre.WeaponScopeUpgrades[DataCentre.CurrentSniper - 1] == 1){
			gun.ZoomFOVLists = new float [4];
			gun.ZoomFOVLists [0] = 30;
			gun.ZoomFOVLists [1] = 20;
			gun.ZoomFOVLists [2] = 10;
			gun.ZoomFOVLists [2] = 5;
		}


		if(DataCentre.WeaponScopeUpgrades[DataCentre.CurrentSniper - 1] == 2){
			gun.ZoomFOVLists = new float [6];
			gun.ZoomFOVLists [0] = 30;
			gun.ZoomFOVLists [1] = 20;
			gun.ZoomFOVLists [2] = 10;
			gun.ZoomFOVLists [2] = 5;
			gun.ZoomFOVLists [2] = 2;
			gun.ZoomFOVLists [2] = 1;
		}
	}
}
