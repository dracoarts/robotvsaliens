﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;
public class TruckBlastHandler : GameController {

	public GameObject BlastedTruck;
	private bool isInRange = false;

	private splineMove move;

	void OnEnable(){
		move = this.GetComponent<splineMove> ();
	}

	void OnTriggerEnter(Collider col){
		Debug.Log ("Enter");
		if (col.gameObject.CompareTag ("Mine")) {
			isInRange = true;
		}
	}

	void OnTriggerExit(Collider col){
		if (col.gameObject.CompareTag ("Mine")) {
			isInRange = false;
		}
	}



	public void BlastThisCar(){
		if (isInRange) {
			move.Stop ();
			DataCentre.UnlockLevels [8] = true;
			DataCentre.Save ();
			BlastedTruck.transform.position = this.transform.position;
			BlastedTruck.SetActive (true);
			DataCentre.UnlockLevels [8] = true;
			DataCentre.Save ();
			Invoke ("TComplete", 2.5f);
			this.gameObject.SetActive (false);
		} else {
			Invoke ("TFail", 2.5f);
		}
	}


	void TComplete(){
		GameManager.Instance.LevelComplete ();
	}

	void TFail(){
		GameManager.Instance.LevelFailed ();
	}


	public override void OnLevelPaused ()
	{
		move.Pause ();
	}

	public override void OnLevelResume ()
	{
		move.Resume ();
	}


	public override void OnLevelStart ()
	{
		move.StartMove ();
	}
}
