﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;
public class FailSplineStoper : GameController {
	private splineMove move;


	void OnEnable(){
		move = this.GetComponent<splineMove> ();
	}


	public override void OnLevelComplete ()
	{
		move.Stop ();
	}
		
	public override void OnLevelPaused ()
	{
		move.Pause ();
	}
	public override void OnLevelResume ()
	{
		move.Resume ();
	}


	public override void OnLevelStart ()
	{
		move.StartMove ();
	}

}
