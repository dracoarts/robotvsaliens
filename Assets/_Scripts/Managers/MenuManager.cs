﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System;
public class MenuManager : Singleton<MenuManager> {
	//*********** All InGame MenuControllers********************************
	[Header("MenuControllers")]
	public IMenuController []MenuControllers;
	[Header("SubMenuControllers")]
	public IMenuController []SubMenus;


	public IMenuController LoadingScreen;

	//*********** Delay Between Switching********************************
	public float SwitchMenuDelay;

	public IMenuController TopStack;


	public PopupController popUp;
	//public PopupMenuController PopupController;




	public void LoadScene(string levelName)
	{
		StartCoroutine (LoadAsyncScene (levelName));

	}
	private IEnumerator LoadAsyncScene(string levelName){
		OnSceneLoading ();
		AsyncOperation op = Application.LoadLevelAsync (levelName);
		yield return new WaitUntil (() => op.isDone);
		OnSceneLoaded ();
	}


	public void LoadScene(int levelNo)
	{
		StartCoroutine (LoadAsyncScene (levelNo));

	}
	private IEnumerator LoadAsyncScene(int levelNo){
		OnSceneLoading ();
		AsyncOperation op = Application.LoadLevelAsync (levelNo);
		yield return new WaitUntil (() => op.isDone);
		OnSceneLoaded ();
	}

#region GameEventsImplementation
	//*****************************  Event Functions *******************************

	public void OnSceneLoading(){
		LoadingScreen.Activate();
	}
	public void OnSceneLoaded(){
		LoadingScreen.Deactivate ();
	}
	public void OnInitialize(){
		TopStack.Activate ();
	}

	//------------------------------ End Event Functions --------------------------
#endregion

	public void SwitchNextMenu(IMenuController currentMenu,IMenuController nextMenu){
		bool isLegal = false;
		foreach(IMenuController temp in currentMenu.LegalMenus){
			if(temp.GetType()==nextMenu.GetType()){
				isLegal=true;
				break;
			}
		}
		if (isLegal){
				TopStack = nextMenu;
				nextMenu.setPreviousMenu(currentMenu);
				currentMenu.IsFocus (false);
				if (!IsSubMenu (nextMenu)) {
					currentMenu.Deactivate ();
				}
				
			StartCoroutine (ActivateMenuWithDelay (nextMenu));
		} else {
	
		}
	}
	public bool SwitchPreviousMenu(IMenuController currentMenu){
		if(currentMenu.getPreviousMenu()!=null){
			if(TopStack.GetType()==currentMenu.GetType()){
				TopStack = currentMenu.getPreviousMenu ();
				currentMenu.Deactivate ();
				currentMenu.IsFocus (false);
				StartCoroutine (ActivateMenuWithDelay (currentMenu.getPreviousMenu ()));
				return true;
			}
		}
		return false;
	}
	bool IsSubMenu(IMenuController next){
		bool isSubMenu = false;
		foreach(IMenuController temp in SubMenus){
			if(temp.GetType()==next.GetType()){
				isSubMenu=true;
				break;
			}
		}
		return isSubMenu;
	}
	IEnumerator ActivateMenuWithDelay(IMenuController nextMenu){
		yield return new WaitForSeconds (SwitchMenuDelay);
		nextMenu.Activate ();
		nextMenu.IsFocus (true);

	}

}


public abstract class IMenuController : GameController{

	public IMenuController []LegalMenus;
	private IMenuController previousMenu=null;



	public virtual void setPreviousMenu(IMenuController prevMenu){
		this.previousMenu = prevMenu;
	}


	public virtual IMenuController getPreviousMenu(){
		return this.previousMenu;
	}
	// *********************************** Activation/Deactivation of Menu ******************************************
	public virtual void Activate(){
		this.gameObject.SetActive (true);
	}
	public virtual void Deactivate(){
		this.gameObject.SetActive (false);
	}
	// ------------------------------------- Activation/Deactivation of Menu -------------------------------------


	//*******************************  Switch Menu *************************************

	public virtual void SwitchNextMenu(IMenuController nextMenu){
		
		MenuManager.Instance.SwitchNextMenu (this, nextMenu);
		GameManager.Instance.PlayButtonClick ();
	}
	public virtual void SwitchPreviousMenu(){
		 MenuManager.Instance.SwitchPreviousMenu (this);
		GameManager.Instance.PlayButtonClick ();
	}
	//*******************************  Switch Menu *************************************



	public virtual void IsFocus(bool focus){
		
	}
	// need to override that menus donot go back
	public virtual void Update(){
		if(Input.GetKeyDown(KeyCode.Escape))
			SwitchPreviousMenu ();
	}


}

