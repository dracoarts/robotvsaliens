﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class StoreManager : Singleton<StoreManager>,IStoreListener {
	private static IStoreController m_StoreController;
	private static IExtensionProvider m_StoreExtensionProvider;


	void Start ()
	{
		if (m_StoreController == null) 
		{
			InitializePurchasing ();
			InvokeRepeating ("InitIAP", 3, 3);
		}
	}

	public void InitializePurchasing ()
	{
		if (IsInitialized ())
			return;

		Debug.Log ("Initializing Purchase...");
		var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());
		builder.AddProduct ("com.taptash.robot.transformer.machine.alien.war.ship.superhero.noads", ProductType.NonConsumable);


		UnityPurchasing.Initialize (this, builder);
	}


	private bool IsInitialized ()
	{
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}
	void InitIAP(){
		InitializePurchasing ();
	}
	public void BuyProduct(string productID)
	{
		BuyProductID (productID);
	}

	void BuyProductID (string productId)
	{
		if (IsInitialized ()) 
		{
			Product product = m_StoreController.products.WithID (productId);

			if (product != null && product.availableToPurchase) 
			{
				m_StoreController.InitiatePurchase (product, "0.99$");
			}

			else 
			{
				Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
		else 
		{
			Debug.Log ("BuyProductID FAIL. Not initialized.");
		}
	}

	public void RestorePurchases ()
	{
		if (!IsInitialized ()) 
		{
			Debug.Log ("RestorePurchases FAIL. Not initialized.");
			return;
		}

		if (Application.platform == RuntimePlatform.IPhonePlayer ||
			Application.platform == RuntimePlatform.OSXPlayer) 
		{
			Debug.Log ("RestorePurchases started ...");
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions> ();
			apple.RestoreTransactions ((result) => {
				Debug.Log ("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		else 
		{
			Debug.Log ("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}


	/****** Unity IAP Callback Events ******/

	public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
	{
		Debug.Log ("OnInitialized: PASS");

		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
	}


	public void OnInitializeFailed (InitializationFailureReason error)
	{
		Debug.Log ("OnInitializeFailed InitializationFailureReason:" + error);
	}


	public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args)
	{
//		if (System.String.Equals (args.purchasedProduct.definition.id, kProductIDs[0], System.StringComparison.Ordinal)) 
//		{
//			DataCentre.NoAds = true;
//			DataCentre.Save ();
//			GAManager.Instance.PurchaseEvent ("USD", 100, "Non Consumable", "test", "");
//		}
		ProcessIAP(args.purchasedProduct.definition.id);
		return PurchaseProcessingResult.Complete;
	}


	public void OnPurchaseFailed (Product product, PurchaseFailureReason failureReason)
	{
		Debug.Log (string.Format ("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}



	void ProcessIAP(string ID){
		switch (ID) {
		case "com.taptash.robot.transformer.machine.alien.war.ship.superhero.noads":
			DataCentre.NoAds = true;
			DataCentre.Save ();
			break;
		}
	}

	/****** Unity IAP Callback Events ******/
}
