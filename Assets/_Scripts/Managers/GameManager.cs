﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class GameManager : Singleton<GameManager> {

	public AudioSource source;

	void OnEnable(){
		 DataCentre.Load();
	}




	public void LevelComplete(){
		GameController[] controllers = GameObject.FindObjectsOfType<GameController>();
		foreach (GameController c in controllers) {
			c.OnLevelComplete ();
		
		}
	}

	public void LevelFailed(){

		GameController[] controllers = GameObject.FindObjectsOfType<GameController>();
		foreach (GameController c in controllers) {
			c.OnLevelFailed ();
		}
	}

	public void LevelPaused(){
		GameController[] controllers = GameObject.FindObjectsOfType<GameController>();
		foreach (GameController c in controllers) {
			c.OnLevelPaused ();
		}
	}

	public void LevelResume(){
		GameController[] controllers = GameObject.FindObjectsOfType<GameController>();
		foreach (GameController c in controllers) {
			c.OnLevelResume ();
		}
	}

	public void LevelStart(){
		GameController[] controllers = GameObject.FindObjectsOfType<GameController>();
		foreach (GameController c in controllers) {
			c.OnLevelStart ();
		}
	}

	public void LevelRestart(){
		GameController[] controllers = GameObject.FindObjectsOfType<GameController>();
		foreach (GameController c in controllers) {
			c.OnLevelRestart ();
		}
	}

	public void LevelRevive(){
		GameController[] controllers = GameObject.FindObjectsOfType<GameController>();
		foreach (GameController c in controllers) {
			c.OnLevelRevive ();
		}
	}

	public void TaskCompleted(){
		GameController[] controllers = GameObject.FindObjectsOfType<GameController>();
		foreach (GameController c in controllers) {
			c.OnTaskComplete ();
		}
	}

	public void LoadScene(int SceneNo){
	}


	public void PlayButtonClick(){
		if(!DataCentre.Sound)
			source.Play ();
	}
	// Can Make Asyc Scene Load If Required

	void OnDestroy(){
		DataCentre.Save ();
	}

	void OnApplicationQuit(){
		DataCentre.Save ();
	}

	void OnApplicationFocus(bool focus){
		DataCentre.Save ();
	}

	void OnDisable(){
		DataCentre.Save ();
	}




}
