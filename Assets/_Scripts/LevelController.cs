﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {

	public GameObject []TotalLevels;
	void OnEnable(){
		Invoke ("ActiveLevel", 0.2f);
	}

	void ActiveLevel(){
		TotalLevels [DataCentre.CurrentLevel - 1].SetActive (true);
		GameManager.Instance.LevelStart ();
	}
}
