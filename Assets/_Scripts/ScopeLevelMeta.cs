﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScopeLevelMeta : MonoBehaviour {

	public GameObject []LevelUpButtons;
	public GameObject []NormalButtons;



	public void Update(){
		if (DataCentre.WeaponScopeUpgrades [DataCentre.CurrentSniper - 1] == 0) {
			LevelUpButtons [0].SetActive (true);
			LevelUpButtons [1].SetActive (true);
			NormalButtons [0].SetActive (false);
			NormalButtons [1].SetActive (false);
		}

		else if (DataCentre.WeaponScopeUpgrades [DataCentre.CurrentSniper - 1] == 1) {
			LevelUpButtons [0].SetActive (false);
			LevelUpButtons [1].SetActive (true);
			NormalButtons [0].SetActive (true);
			NormalButtons [1].SetActive (false);
		}

		else if (DataCentre.WeaponScopeUpgrades [DataCentre.CurrentSniper - 1] == 2) {
			LevelUpButtons [0].SetActive (false);
			LevelUpButtons [1].SetActive (false);
			NormalButtons [0].SetActive (true);
			NormalButtons [1].SetActive (true);
		}
	}
}
