﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : GameController {
	public float TotalKills;

	public float TotalTime;

	private bool isLevelComplete = false;
	private bool isGamePaused = false;
	void OnEnable(){
		Invoke ("SetMap", 0.2f);
	}
	void SetMap(){
		GameObject.FindObjectOfType<bl_MiniMap> ().SetTarget (GameObject.FindGameObjectWithTag ("Player"));
		StartCoroutine (Timmer ());
		GameManager.Instance.LevelStart ();
	}
	public void FinishBreifing(){
		Time.timeScale = 1;
	
	}


	public void TriggerKill(){
		TotalKills--;
		if (TotalKills <= 0) {
			isLevelComplete = true;
			Invoke ("LevelComplete", 2.5f);
		}
	}
	void LevelComplete(){
		DataCentre.UnlockLevels [Mathf.Clamp (DataCentre.CurrentLevel, 0, 9)] = true;
		GameManager.Instance.LevelComplete ();
	}

	public void ManualLevelComplete(){
		isLevelComplete = true;
		Invoke ("LevelComplete", 2.5f);
	}

	public void LevelFailtriiger(){
		Debug.Log ("Level Faillled");
		isLevelComplete = true;
		GameManager.Instance.LevelFailed ();
	}
	public override void OnLevelPaused (){
		isGamePaused = true;
	}

	public override void OnLevelResume(){
		isGamePaused = false;
	}

	IEnumerator Timmer(){
		while(TotalTime>0){
			yield return new WaitForSecondsRealtime (1);
			yield return new WaitWhile (()=>isGamePaused);
			TotalTime--;
		}	

		if (!isLevelComplete) {
			isLevelComplete = true;
			GameManager.Instance.LevelFailed ();
		}
	}
}
