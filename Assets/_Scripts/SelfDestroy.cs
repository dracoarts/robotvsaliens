﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroy : MonoBehaviour {

	public float timeToDestroy;

	void OnEnable(){
		Destroy (this.gameObject, timeToDestroy);
	}
}
