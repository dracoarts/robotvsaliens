﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBuyMeta : MonoBehaviour {

	public GameObject Level;
	public void Update(){
		if (DataCentre.unlockedWeapons [DataCentre.CurrentSniper - 1]) {
			Level.SetActive (true);
		} else {
			Level.SetActive (false);
		}	
	}
}
