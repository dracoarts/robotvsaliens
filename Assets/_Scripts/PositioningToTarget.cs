﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositioningToTarget : MonoBehaviour {

	public string TagToFollow;
	private GameObject Target;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Target == null) {
			Target = GameObject.FindGameObjectWithTag (TagToFollow) as GameObject;
		} else {
			this.gameObject.transform.position = Target.transform.position;
		}
	}
}
