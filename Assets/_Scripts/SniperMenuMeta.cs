﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperMenuMeta : MonoBehaviour {

	public GameObject []Snipers;


	void DisbaleAllSnipers()
	{
		foreach (GameObject g in Snipers) {
			g.SetActive (false);
		}
	}


	public void ActivateSniper(int ID){
		DisbaleAllSnipers ();
		Snipers [ID - 1].SetActive (true);
	}
}
