﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillTrigger : MonoBehaviour {

	private DamageManager manager;
	private Level level;
	void OnEnable(){
		manager = this.GetComponent<DamageManager> ();
		level = GameObject.FindObjectOfType<Level> ();
	}

	void Update(){
//		if (manager.hp <= 0) {
//			level.TriggerKill ();
//			Destroy (this);
//		}
	}


	void OnDisable(){
		level.TriggerKill ();
		Destroy (this);
	}
}
