﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DinoController : MonoBehaviour {

	public int CurrentHealth;
	public int StartingHealth;

	public int BulltetHitImpact;
	public int MissileHitImpact;

	private Emerald_Animal_AI animal;

	public Image fillImage;
	public GameObject Bar;
	private Level currentLevel;
	 
	void OnEnable(){
		currentLevel = GameObject.FindObjectOfType<Level> ();
		animal = this.GetComponent<Emerald_Animal_AI> ();
		animal.startingHealth = StartingHealth;
		animal.currentHealth = CurrentHealth;
	}
	public void MissileHit(){
		animal.currentHealth -= MissileHitImpact;
		if (animal.currentHealth <= 0) {
			Die ();
		}
	
	}


	public void BullletHit(){
		animal.currentHealth -= BulltetHitImpact;
		if (animal.currentHealth <= 0) {
			Die ();
		}
	}
	void Update(){
		if (fillImage != null) {
			fillImage.fillAmount = ((float)animal.currentHealth / (float)StartingHealth);
		}
	}

	void Die(){
		Bar.SetActive (false);
		currentLevel.TriggerKill ();
		Destroy (this.gameObject, 2);
	}
}
