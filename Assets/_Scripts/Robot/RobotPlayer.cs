﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;
public class RobotPlayer : MonoBehaviour {


	public AudioSource FireSound;
	public AudioSource MissileSound;
	public AudioClip MissileClip;
	private bool isGameRunning = true;
	public string EnemyTag;
	public string BigFireFunction;
	public string FireFunction;


	public float MissileCoolDownTime;
	private CharacterController controller;
	private Animator _animator;

	public GameObject firePoint;
	public float totalHealth = 100;
	private float playerHitThrshhold = 7;
	public float totalMissles = 5;
	public float totalBulltets = 5000; 
	public float playerSpeed;
	public float runningSpeed;
	public float turnSpeed = 1;



	public Texture crossHair;
	public GameObject MissileHitEffect;
	public GameObject MissileExplosionEffect;
	public GameObject leftHandEffect;
	public GameObject rightHandEffect;
	public Animator _leftArmAnimator;
	public Animator _rightArmAnimator;

	private bool Moving = false;
	private bool MovingForward = false;
	private bool LastMovingState = false;
	private bool isFiring = false;
	private bool isRunning = false;
	private bool lockMissile = false;

	void OnEnable(){
		isGameRunning = true;
		playerSpeed /= 1000;
		runningSpeed /= 1000;
		controller = this.GetComponent<CharacterController> ();
		_animator = this.GetComponent<Animator> ();
	}


	private void AnimationsControls(){
		if (Input.GetKey (KeyCode.UpArrow) || ControlFreak2.CF2Input.GetAxisRaw("Vertical")>0) {
			Moving = true;
			MovingForward = true;
		} else if (Input.GetKey (KeyCode.LeftArrow) || ControlFreak2.CF2Input.GetAxisRaw("Horizontal")<0f) {
			Moving = true;
			MovingForward = true;
		} else if (Input.GetKey (KeyCode.RightArrow) || ControlFreak2.CF2Input.GetAxisRaw("Horizontal")>0f) {
			Moving = true;
			MovingForward = true;
		} else if(Input.GetKey (KeyCode.DownArrow) || ControlFreak2.CF2Input.GetAxisRaw("Vertical")<0){
			Moving = true;
			MovingForward = false;
		}else {
			Moving = false;
			MovingForward = false;
		}

		_animator.SetBool ("Run",isRunning);

		if (LastMovingState) {
			if (!Moving) {
				LastMovingState = false;
				_animator.SetTrigger ("Idle");
			} 
		} else {
			if (Moving) {
				LastMovingState = true;

				if (MovingForward) {
					_animator.SetTrigger ("Walk");
				}else {
					_animator.SetBool("Run",false);
					_animator.SetTrigger ("Backwalk");
				}
			}
		}
	
	}


	private void Inputs(){


		isRunning = Input.GetKey (KeyCode.LeftShift);
		if (Input.GetKey (KeyCode.UpArrow)) {
			Walk ();
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			BackwardWalk ();
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			RotateRight ();
		} else if (Input.GetKey (KeyCode.LeftArrow)) {
			RotateLeft ();
		}
	

		if (Input.GetKeyDown (KeyCode.F)) {
			Fire (true);
		} else if (Input.GetKeyUp (KeyCode.F)) {
			Fire (false);
		}

		if (Input.GetKeyDown (KeyCode.M) && !lockMissile && totalMissles>0) {
			lockMissile = true;
			Invoke ("UnlockMissile", MissileCoolDownTime);
			FireMissile ();
		}

		DoFire ();

	}
	private void MobileInputs(){
		
		if (ControlFreak2.CF2Input.GetAxisRaw ("Vertical") >= 0.9f) {
			isRunning = true;
		} else {
			isRunning = false;
		}

		if (ControlFreak2.CF2Input.GetAxisRaw("Vertical")>0) {
			Walk ();
		}
		else if (ControlFreak2.CF2Input.GetAxisRaw("Vertical")<0) {
			BackwardWalk ();
		}

		if (ControlFreak2.CF2Input.GetAxisRaw("Horizontal")>0.3f) {
			RotateRight ();
		} else if (ControlFreak2.CF2Input.GetAxisRaw("Horizontal")<-0.3f) {
			RotateLeft ();
		}


		if (ControlFreak2.CF2Input.GetKeyDown (KeyCode.F)) {
			Fire (true);
		} else if (ControlFreak2.CF2Input.GetKeyUp (KeyCode.F)) {
			Fire (false);
		}

		if (ControlFreak2.CF2Input.GetKeyDown (KeyCode.M) && !lockMissile && totalMissles>0) {
			lockMissile = true;
			Invoke ("UnlockMissile", MissileCoolDownTime);
			FireMissile ();
		}

		DoFire ();
	}
	void UnlockMissile(){
		lockMissile = false;
	}

	void DoFire(){
		if (isFiring && totalBulltets>0) {
			totalBulltets--;
			CameraShaker.Instance.ShakeOnce(1f,0.5f,0,0.5f);
			Ray ray = new Ray (firePoint.transform.position, firePoint.transform.forward);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 200)) {
				print ("I'm looking at " + hit.transform.name);
				if (hit.collider.gameObject.CompareTag (EnemyTag)) {
					hit.collider.gameObject.SendMessage (FireFunction);
				}
			}
		}
	}
	public void DamagePlayer(){
		/*
		 * Player Damage Fucntion call when ever enemy SendMessage of DamagePlayer
		 * Can be Change from AI
		 */
		totalHealth -= playerHitThrshhold;
		if (totalHealth <= 0) {
			// Die
			isGameRunning = false;
			_animator.SetTrigger("Die");
		}

		Debug.Log ("Player is Damaging");
	}
	private void Walk(){
		float Speed = (isRunning) ? runningSpeed : playerSpeed;
		controller.Move (this.transform.forward *  Speed);
	}
	private void BackwardWalk(){
		float Speed = playerSpeed;
		Speed *= -1;
		controller.Move (this.transform.forward *  Speed);
	}
	private void RotateLeft(){
		this.transform.Rotate(new Vector3(0,-turnSpeed,0));
	}
	private void RotateRight(){
		this.transform.Rotate(new Vector3(0,turnSpeed,0));
	}
	private void Fly(){
	}
	private void Land(){
	}
	private void Fire(bool value){

		leftHandEffect.SetActive (value);
		rightHandEffect.SetActive (value);

		_leftArmAnimator.SetBool ("Fire", value);
		_rightArmAnimator.SetBool ("Fire", value);

		// robot is Firing
		isFiring = value;

		FireSound.mute = !isFiring;

	}
	private void FireMissile(){
		CameraShaker.Instance.ShakeOnce(5, 10, 0, 3);
		Fire (false);
		MissileSound.PlayOneShot (MissileClip);
		Instantiate (MissileExplosionEffect, leftHandEffect.transform.position, Quaternion.identity);
		Instantiate (MissileExplosionEffect, rightHandEffect.transform.position, Quaternion.identity);
		_leftArmAnimator.SetTrigger ("Missile");
		_rightArmAnimator.SetTrigger ("Missile");
		totalMissles--;
		Ray ray = new Ray(firePoint.transform.position,firePoint.transform.forward);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, 200)) {
			Instantiate (MissileHitEffect, hit.point, Quaternion.identity);
			print ("I'm looking at " + hit.transform.name);

			if (hit.collider.gameObject.CompareTag (EnemyTag)) {
				hit.collider.gameObject.SendMessage (BigFireFunction);
			}
		}

	}

	void OnGUI() {
		GUI.DrawTexture (new Rect ((Screen.width / 2) - (25/2), (Screen.height / 5) - (25/2), 25, 25), crossHair);
	}
	void Update(){
		if (isGameRunning) {
			//Inputs ();
			MobileInputs ();
		}
	}

	void LateUpdate(){
		if (isGameRunning) {
			AnimationsControls ();
		}
	}

}
