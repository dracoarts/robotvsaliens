﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKillLevelFail : MonoBehaviour {

	private DamageManager manager;
	void OnEnable(){
		manager = this.GetComponent<DamageManager> ();
	}


	void Update(){
		if (manager.hp <= 0) {
			GameManager.Instance.LevelFailed ();
			Destroy (this);
		}
	}
}

