﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DailyRewardController : IMenuController {

	public GameObject []collectableGameObjects;
	public Button[] collectableButtons;
	public Text[] Timers;
	private System.DateTime LastRewardClaimTime;


	public int []RewardAmount;

	void OnEnable(){
		Debug.Log ("In There");
		System.DateTime.TryParse(DataCentre.LastRewardClaimedTime,out LastRewardClaimTime);
		DisableAllCollectables ();
		InitCurrentCollectable ();
	}

	void DisableAllCollectables(){
		foreach (GameObject g in collectableGameObjects) {
			g.SetActive (false);
		}
		foreach (Button b in collectableButtons) {
			b.interactable = false;
		}
		foreach (Text t in Timers) {
			t.text = "";
		}
	}

	void InitCurrentCollectable(){
		
		if (System.DateTime.Now - LastRewardClaimTime >= new System.TimeSpan (24, 0, 0) || DataCentre.totalRewardClaimed == 0) {
			collectableButtons [DataCentre.totalRewardClaimed].interactable = true;
			collectableGameObjects [DataCentre.totalRewardClaimed].SetActive (true);
			Timers [DataCentre.totalRewardClaimed].text = "";
		} else {
			Timers [DataCentre.totalRewardClaimed].text = (new System.TimeSpan(24,0,0)-(System.DateTime.Now - LastRewardClaimTime)).ToString();
		}
	}


	void Update(){
		InitCurrentCollectable ();
	}
	public void CollectReward(){
		DisableAllCollectables ();
		LastRewardClaimTime = System.DateTime.Now;
		DataCentre.LastRewardClaimedTime = System.DateTime.Now.ToString ();
		DataCentre.Coins += RewardAmount [DataCentre.totalRewardClaimed];
		DataCentre.totalRewardClaimed = Mathf.Clamp (DataCentre.totalRewardClaimed + 1, 0, 7);

	}


	

}
