using UnityEngine;
using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;
using System.Security.Cryptography;
using System.Linq;
using System.Text;
using System.IO.Compression;
namespace PreviewLabs
{
	public static class PlayerPrefs
	{
		private const int Keysize = 256;
		private const int DerivationIterations = 1000;
		private static Hashtable playerPrefsHashtable = new Hashtable();
		
		private static bool hashTableChanged = false;
		private static string serializedOutput = "";
		private static string serializedInput = "";
		
		private const string PARAMETERS_SEPERATOR = ";";
		private const string KEY_VALUE_SEPERATOR = ":";
		
		private static readonly string fileName = Path.Combine (Application.persistentDataPath, "a.bin");
		
		
		static PlayerPrefs()
		{
			//load previous settings
			StreamReader fileReader = null;
			if (File.Exists(fileName))
			{
				
				fileReader = new StreamReader(fileName);
				
				serializedInput = fileReader.ReadLine();
			//	serializedInput = Decrypt (serializedInput);
				Debug.Log (serializedInput.ToString ());
				Deserialize();
				
				fileReader.Close();
			}
		}
		
		public static bool HasKey(string key)
		{			
			return playerPrefsHashtable.ContainsKey(key);
		}
		
		public static void SetString(string key, string value)
		{
			
			if(!playerPrefsHashtable.ContainsKey(key))
			{
				playerPrefsHashtable.Add(key, value);
			}
			else
			{
				playerPrefsHashtable[key] = value;
			}
			
			hashTableChanged = true;
		}
		
		public static void SetInt(string key, int value)
		{
			if(!playerPrefsHashtable.ContainsKey(key))
			{
				playerPrefsHashtable.Add(key, value);
			}
			else
			{
				playerPrefsHashtable[key] = value;
			}
			
			hashTableChanged = true;
		}
		
		public static void SetFloat(string key, float value)
		{
			if(!playerPrefsHashtable.ContainsKey(key))
			{
				playerPrefsHashtable.Add(key, value);
			}
			else
			{
				playerPrefsHashtable[key] = value;
			}
			
			hashTableChanged = true;
		}
		
		public static void SetBool(string key, bool value)
		{
			if(!playerPrefsHashtable.ContainsKey(key))
			{
				playerPrefsHashtable.Add(key, value);
			}
			else
			{
				playerPrefsHashtable[key] = value;
			}
			
			hashTableChanged = true;
		}
		
		public static string GetString(string key)
		{			
			if(playerPrefsHashtable.ContainsKey(key))
			{
				return playerPrefsHashtable[key].ToString();
			}
			
			return null;
		}
		
		public static string GetString(string key, string defaultValue)
		{
			if(playerPrefsHashtable.ContainsKey(key))
			{
				return playerPrefsHashtable[key].ToString();
			}
			else
			{
				playerPrefsHashtable.Add(key, defaultValue);
				hashTableChanged = true;
				return defaultValue;
			}
		}
		
		public static int GetInt(string key)
		{			
			if(playerPrefsHashtable.ContainsKey(key))
			{
				return (int) playerPrefsHashtable[key];
			}
			
			return 0;
		}
		
		public static int GetInt(string key, int defaultValue)
		{
			if(playerPrefsHashtable.ContainsKey(key))
			{
				return (int) playerPrefsHashtable[key];
			}
			else
			{
				playerPrefsHashtable.Add(key, defaultValue);
				hashTableChanged = true;
				return defaultValue;
			}
		}
		
		public static float GetFloat(string key)
		{			
			if(playerPrefsHashtable.ContainsKey(key))
			{
				return (float) playerPrefsHashtable[key];
			}
			
			return 0.0f;
		}
		
		public static float GetFloat(string key, float defaultValue)
		{
			if(playerPrefsHashtable.ContainsKey(key))
			{
				return (float) playerPrefsHashtable[key];
			}
			else
			{
				playerPrefsHashtable.Add(key, defaultValue);
				hashTableChanged = true;
				return defaultValue;
			}
		}
		
		public static bool GetBool(string key)
		{			
			if(playerPrefsHashtable.ContainsKey(key))
			{
				return (bool) playerPrefsHashtable[key];
			}
			
			return false;
		}
		
		public static bool GetBool(string key, bool defaultValue)
		{
			if(playerPrefsHashtable.ContainsKey(key))
			{
				return (bool) playerPrefsHashtable[key];
			}
			else
			{
				playerPrefsHashtable.Add(key, defaultValue);
				hashTableChanged = true;
				return defaultValue;
			}
		}
		
		public static void DeleteKey(string key)
		{
			playerPrefsHashtable.Remove(key);
		}
		
		public static void DeleteAll()
		{
			playerPrefsHashtable.Clear();
		}
		
		public static void Flush()
		{
			if(hashTableChanged)
			{
				Serialize();
				StreamWriter fileWriter = null;
				fileWriter = File.CreateText(fileName);
			
				if (fileWriter == null)
				{ 
					Debug.LogWarning("PlayerPrefs:Flush() opening file for writing failed: " + fileName);
				}

				//serializedOutput=Encrypt (serializedOutput);
				fileWriter.WriteLine(serializedOutput);
				fileWriter.Flush ();
				fileWriter.Close();
				serializedOutput = "";
			}
		
		}
		
		private static void Serialize()
		{
			IDictionaryEnumerator myEnumerator = playerPrefsHashtable.GetEnumerator();
			
			while ( myEnumerator.MoveNext() )
			{
				if(serializedOutput != "")
				{
					serializedOutput += " " + PARAMETERS_SEPERATOR + " ";
				}
				serializedOutput += EscapeNonSeperators(myEnumerator.Key.ToString()) + " " + KEY_VALUE_SEPERATOR + " " + EscapeNonSeperators(myEnumerator.Value.ToString()) + " " + KEY_VALUE_SEPERATOR + " " + myEnumerator.Value.GetType();
			}
		}
		
		private static void Deserialize()
		{
			string[] parameters = serializedInput.Split(new string[] {" " + PARAMETERS_SEPERATOR + " "}, StringSplitOptions.None);
			
			foreach(string parameter in parameters)
			{
				string[] parameterContent = parameter.Split(new string[]{" " + KEY_VALUE_SEPERATOR + " "}, StringSplitOptions.None);
				
				playerPrefsHashtable.Add(DeEscapeNonSeperators(parameterContent[0]), GetTypeValue(parameterContent[2], DeEscapeNonSeperators(parameterContent[1])));
				
				if(parameterContent.Length > 3)
				{
					Debug.LogWarning("PlayerPrefs:Deserialize() parameterContent has " + parameterContent.Length + " elements");
				}
			}
		}
		
		private static string EscapeNonSeperators(string inputToEscape)
		{
			inputToEscape = inputToEscape.Replace(KEY_VALUE_SEPERATOR,"\\" + KEY_VALUE_SEPERATOR);
			inputToEscape = inputToEscape.Replace(PARAMETERS_SEPERATOR,"\\" + PARAMETERS_SEPERATOR);
			return inputToEscape;
		}
		
		private static string DeEscapeNonSeperators(string inputToDeEscape)
		{
			inputToDeEscape = inputToDeEscape.Replace("\\" + KEY_VALUE_SEPERATOR, KEY_VALUE_SEPERATOR);
			inputToDeEscape = inputToDeEscape.Replace("\\" + PARAMETERS_SEPERATOR, PARAMETERS_SEPERATOR);
			return inputToDeEscape;
		}
		
		public static object GetTypeValue(string typeName, string value)
		{
			if (typeName == "System.String")
			{
				return (object)value.ToString();
			}
			if (typeName == "System.Int32")
			{
				return (object)System.Convert.ToInt32(value);
			}
			if (typeName == "System.Boolean")
			{
				return (object)System.Convert.ToBoolean(value);
			}
			if (typeName == "System.Single")// -> single = float
			{
				return (object)System.Convert.ToSingle(value);
			}
			else
			{
				Debug.LogError("Unsupported type: " + typeName);
			}	
			
			return null;
		}
	
	
	
	
	


		public static string Encrypt(string plainText)
		{



			byte[] keyArray = UTF8Encoding.UTF8.GetBytes ("SecretDraco");
			// 256-AES key
			int numBytes = System.Text.Encoding.UTF8.GetBytes(plainText).Length;
			Debug.Log ("Bytes: " + numBytes);
			byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes (plainText);
			RijndaelManaged rDel = new RijndaelManaged ();
			rDel.Key = keyArray;
			rDel.IV = rDel.Key;
			rDel.BlockSize = 128;
			rDel.Mode = CipherMode.CBC;
			// http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
			rDel.Padding = PaddingMode.PKCS7;
			// better lang support
			ICryptoTransform cTransform = rDel.CreateEncryptor ();
			byte[] resultArray = cTransform.TransformFinalBlock (toEncryptArray, 0, toEncryptArray.Length);
			return Convert.ToBase64String (resultArray, 0, resultArray.Length);










//			string EncryptionKey = "MAKV2SPBNI99212";
//			byte[] clearBytes = Encoding.Unicode.GetBytes(plainText);
//			using (Aes encryptor = Aes.Create())
//			{
//				Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, 32);
//				encryptor.Key = pdb.GetBytes(32);
//				encryptor.IV = pdb.GetBytes(16);
//				encryptor.Padding = PaddingMode.PKCS7;
//				using (MemoryStream ms = new MemoryStream())
//				{
//					using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
//					{
//						cs.Write(clearBytes, 0, clearBytes.Length);
//						cs.Close();
//					}
//					plainText = Convert.ToBase64String(ms.ToArray());
//				}
//			}
//			return plainText;
		}

		public static string Decrypt(string cipherText)
		{






			byte[] keyArray = UTF8Encoding.UTF8.GetBytes ("SecretDraco");
			// AES-256 key
			byte[] encryptedData = System.Convert.FromBase64String(cipherText);
			//byte[] toEncryptArray = Convert.FromBase64String (toDecrypt);
			RijndaelManaged rDel = new RijndaelManaged ();
			rDel.Key = keyArray;
			rDel.IV = rDel.Key;
			rDel.BlockSize = 128;
			rDel.Mode = CipherMode.CBC;
			// http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
			rDel.Padding = PaddingMode.PKCS7;
			// better lang support
			ICryptoTransform cTransform = rDel.CreateDecryptor ();
			byte[] resultArray = cTransform.TransformFinalBlock (encryptedData, 0, encryptedData.Length);
			return UTF8Encoding.UTF8.GetString (resultArray);




//			string EncryptionKey = "MAKV2SPBNI99212";
//			byte[] cipherBytes = Convert.FromBase64String(cipherText);
//			using (Aes encryptor = Aes.Create())
//			{
//				Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, 32);
//				encryptor.Key = pdb.GetBytes(32);
//				encryptor.IV = pdb.GetBytes(16);
//				encryptor.Padding = PaddingMode.PKCS7;
//				using (MemoryStream ms = new MemoryStream())
//				{
//					using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
//					{
//						cs.Write(cipherBytes, 0, cipherBytes.Length);
//						cs.Close();
//					}
//					cipherText = Encoding.Unicode.GetString(ms.ToArray());
//				}
//			}
//			return cipherText;
		}

	
	
	
	
	
	
	
	
	
	
	}	
		
}