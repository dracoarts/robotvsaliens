
using UnityEngine;
using System.Collections;
using System;

public class DataCentre : MonoBehaviour 
{
	#region GAME_CENTRAL_VARIABLE

	public static int CurrentWeaponID = 0;
	public static int []CurrentWeaponsArmed = {0,2};
	public static int CurrentLevel = 1;
	public static int CurrentSniper = 1;
	public static int VehicleController = 2;
	public static int LevelCompleteCounters = 0;
	public static int StarsEarned = 0;

	public static bool Sound = true;
	public static bool Music = true;

	public static float TimerMeta;
	public static float EnemyKilled = 0;

	public static string GAME_URL = "https://play.google.com/store/apps/details?id=com.taptash.robot.transformer.machine.alien.war.ship.superhero";
	#endregion


	#region GAME_CENTRAL_CONSTANTS
	public static int MAX_LIVES = 5;
	public static int[] WeaponPrices = { 0, 1500, 4500 };
	public static int []WeaponUpgradePrices = {0,500,1000};

	#endregion


	#region SAVE_GAME_DATA
	public static string LastRewardClaimedTime = (System.DateTime.Now) .ToString();
	public static int totalRewardClaimed = 0;
	public static bool NoAds = false;
	public static bool showTutorial = true;
	public static int Coins = 500;
	public static int[] WeaponScopeUpgrades = { 0, 0, 0 };
	public static int[] WeaponRangeUpgrades = { 0, 0, 0 };

	public static bool []unlockedWeapons = {true,false,false};
	public static bool []UnlockLevels = { true, false , false, false, false, false, false, false, false, false};
	public static bool []UnlockVehicles = {true, false , false, false, false, false};
	#endregion

	#region SaveLoad_Game_Funtion
	public static void Save ( )
	{
		PreviewLabs.PlayerPrefs.SetString ("LastRewardClaimedTime", LastRewardClaimedTime);
		PreviewLabs.PlayerPrefs.SetBool ("NoAds", NoAds);
		PreviewLabs.PlayerPrefs.SetBool ("showTutorial", showTutorial);
		PreviewLabs.PlayerPrefs.SetInt ("Coins", Coins);
		PreviewLabs.PlayerPrefs.SetInt ("totalRewardClaimed", totalRewardClaimed);
		SaveArray (UnlockLevels, "UnlockLevels");
		SaveArray (UnlockVehicles, "UnlockVehicles");
		SaveArray (unlockedWeapons, "unlockedWeapons");
		SaveArray (WeaponScopeUpgrades, "WeaponScopeUpgrades");
		SaveArray (WeaponRangeUpgrades, "WeaponRangeUpgrades");
		PreviewLabs.PlayerPrefs.Flush ();
	}
	public static void Load ( )
	{
		LoadArray (UnlockLevels, "UnlockLevels");
		LoadArray (UnlockVehicles, "UnlockVehicles");
		LoadArray (unlockedWeapons, "unlockedWeapons");
		LoadArray (WeaponScopeUpgrades, "WeaponScopeUpgrades");
		LoadArray (WeaponRangeUpgrades, "WeaponRangeUpgrades");
		LastRewardClaimedTime = PreviewLabs.PlayerPrefs.GetString ("LastRewardClaimedTime", LastRewardClaimedTime);
		Coins = PreviewLabs.PlayerPrefs.GetInt ("Coins", Coins);
		totalRewardClaimed = PreviewLabs.PlayerPrefs.GetInt ("totalRewardClaimed", totalRewardClaimed);
		showTutorial = PreviewLabs.PlayerPrefs.GetBool ("showTutorial", showTutorial);
		NoAds = PreviewLabs.PlayerPrefs.GetBool ("NoAds", NoAds);

	}
	#endregion 


	static void SaveArray(bool []array,string tag){
		for (int i = 0; i < array.Length; i++) {
			PreviewLabs.PlayerPrefs.SetBool (tag + i, array [i]);
		}
	}
	static void SaveArray(int []array,string tag){
		for (int i = 0; i < array.Length; i++) {
			PreviewLabs.PlayerPrefs.SetInt (tag + i, array [i]);
		}
	}
	static void SaveArray(float []array,string tag){
		for (int i = 0; i < array.Length; i++) {
			PreviewLabs.PlayerPrefs.SetFloat (tag + i, array [i]);
		}
	}
	static void SaveArray(string []array,string tag){
		for (int i = 0; i < array.Length; i++) {
			PreviewLabs.PlayerPrefs.SetString (tag + i, array [i]);
		}
	}


	static void LoadArray(bool [] array,string tag){
		for (int i = 0; i < array.Length; i++) {
			array[i] = PreviewLabs.PlayerPrefs.GetBool (tag + i, array [i]);
		}
	}
	static void LoadArray(int [] array,string tag){
		for (int i = 0; i < array.Length; i++) {
			array[i] = PreviewLabs.PlayerPrefs.GetInt (tag + i, array [i]);
		}
	}
	static void LoadArray(float [] array,string tag){
		for (int i = 0; i < array.Length; i++) {
			array[i] = PreviewLabs.PlayerPrefs.GetFloat (tag + i, array [i]);
		}
	}
	static void LoadArray(string [] array,string tag){
		for (int i = 0; i < array.Length; i++) {
			array[i] = PreviewLabs.PlayerPrefs.GetString (tag + i, array [i]);
		}
	}
}
